package com.ecommerce.client;

import java.util.List;

public class ClientService {

//	ClientConsoleReader clientReader = new ClientConsoleReader();
//	ClientStore clientStore = new ClientStore();

	private ClientDaoImpl clientDao;

	// facem constructor privat
	private ClientService() {
		clientDao = new ClientDaoImpl();
	}

	// singleton, o singura instanta per clasa
	private static ClientService clientService = new ClientService();

	public static ClientService getInstance() {
		return clientService;
	}

	public void addClient(Client client) {
		clientDao.openCurrentSessionwithTransaction();
		clientDao.saveClient(client);
		clientDao.closeCurrentSessionwithTransaction();
	}

	public void deleteClient(int clientID) {
		clientDao.openCurrentSessionwithTransaction();
		Client client = clientDao.findClientByID(clientID);
		clientDao.deleteClient(client);
		clientDao.closeCurrentSessionwithTransaction();
	}

	public Client findClientById(int id) {
		clientDao.openCurrentSession();
		Client client = clientDao.findClientByID(id);
		clientDao.closeCurrentSession();
		return client;
	}

	public void updateClient(Client client) {
		clientDao.openCurrentSessionwithTransaction();
		clientDao.updateClient(client);
		clientDao.closeCurrentSessionwithTransaction();
	}

	public List<Client> getAllClients() {
		clientDao.openCurrentSession();
		List<Client> books = clientDao.findAllClients();
		clientDao.closeCurrentSession();
		return books;
	}

	public void deleteAllClients() {
		List<Client> clientList = getAllClients();
		for (Client client : clientList) {
			deleteClient(client.getIdClient());
		}
	}

}
