package com.ecommerce.client;

import java.util.List;

public interface ClientDAO {

	public void saveClient(Client client);

	public List<Client> findAllClients();

	public Client findClientByID(int clientID);

	public void updateClient(Client client);

	public void deleteClient(Client client);
	
	public void deleteAllClients();
}
