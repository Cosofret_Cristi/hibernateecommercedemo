package com.ecommerce.client;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class ClientDaoImpl implements ClientDAO {

	private Session currentSession;
	private Transaction currentTransaction;

	public ClientDaoImpl() {
	}

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
		return sessionFactory;
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	@Override
	public void saveClient(Client client) {
		getCurrentSession().save(client);
	}

	@Override
	public List<Client> findAllClients() {
		List<Client> clients = (List<Client>) getCurrentSession().createQuery("from Client").list();
		return clients;
	}

	@Override
	public Client findClientByID(int clientID) {
		Client client = (Client) getCurrentSession().get(Client.class, clientID);
		return client;
	}

	@Override
	public void updateClient(Client client) {
		getCurrentSession().update(client);
	}

	@Override
	public void deleteClient(Client client) {
		getCurrentSession().delete(client);
	}

	@Override
	public void deleteAllClients() {
		List<Client> entityList = findAllClients();
		for (Client entity : entityList) {
			deleteClient(entity);
		}
	}

}
