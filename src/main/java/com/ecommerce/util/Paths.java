package com.ecommerce.util;

public class Paths {

    private static final String FILE_PATH = "D:/WS/ecommerce/";
    public static final String CLIENTS_FILE_PATH = FILE_PATH + "clientList.json";
    public static final String PRODUCTS_FILE_PATH = FILE_PATH + "productList.json";
    public static final String ORDERS_FILE_PATH = FILE_PATH + "orderList.json";
}
