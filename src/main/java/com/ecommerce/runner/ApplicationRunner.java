package com.ecommerce.runner;

import java.util.List;

import com.ecommerce.client.Client;
import com.ecommerce.client.ClientService;

public class ApplicationRunner {

	public static void main(String[] args) {

		ClientService clientService = ClientService.getInstance();
		Client firstClient = new Client(1, "Fyodor", "Dostoevsky", "Iasi", "f.d@gmail.com", 1234, "RO10");
		Client secondClient = new Client(2, "Leo", "Tolstoy", "Iasi", "l.t@gmail.com", 4321, "RO11");
		Client lastClient = new Client(3, "Jane", "Austen", "Iasi", "j.a@gmail.com", 2143, "RO12");

		System.out.println("*** Persist - start ***");
		clientService.addClient(firstClient);
		clientService.addClient(secondClient);
		clientService.addClient(lastClient);
		List<Client> clientList = clientService.getAllClients();
		System.out.println("Persisted clients are :");
		for (Client client : clientList) {
			System.out.println("-" + client.toString());
		}
		System.out.println("*** Persist - end ***");

		System.out.println("*** Update - start ***");
		firstClient.setPhone(999);
		clientService.updateClient(firstClient);
		System.out.println("Updated client is =>" + clientService.findClientById(firstClient.getIdClient()).toString());
		System.out.println("*** Update - end ***");

		System.out.println("*** Find - start ***");
		int secondClientID = secondClient.getIdClient();
		Client another = clientService.findClientById(secondClientID);
		System.out.println("Client found with id " + secondClientID + " is =>" + another.toString());
		System.out.println("*** Find - end ***");

		System.out.println("*** Delete - start ***");
		int lastClientID = lastClient.getIdClient();
		clientService.deleteClient(lastClientID);
		System.out.println("Deleted client with id " + lastClientID + ".");
		System.out.println("Now all clients are " + clientService.getAllClients().size() + ".");
		System.out.println("*** Delete - end ***");

		System.out.println("*** FindAll - start ***");
		List<Client> updatedClientList = clientService.getAllClients();
		System.out.println("Clients found are :");
		for (Client client : updatedClientList) {
			System.out.println("-" + client.toString());
		}
		System.out.println("*** FindAll - end ***");

		System.out.println("*** DeleteAll - start ***");
		clientService.deleteAllClients();
		System.out.println("Clients found are now " + clientService.getAllClients().size());
		System.out.println("*** DeleteAll - end ***");
		System.exit(0);

	}
}
