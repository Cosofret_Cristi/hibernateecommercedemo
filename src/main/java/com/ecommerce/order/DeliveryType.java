package com.ecommerce.order;

public enum DeliveryType {
    CURRIER, PERSONAL_COLLECTING;
}
