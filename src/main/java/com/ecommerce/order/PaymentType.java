package com.ecommerce.order;

public enum PaymentType {
    CARD, CASH_ON_DELIVERY;
}
