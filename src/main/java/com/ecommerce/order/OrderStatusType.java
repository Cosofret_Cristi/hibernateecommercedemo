package com.ecommerce.order;

public enum OrderStatusType {

    CANCELED, COMPLETED, IN_PROGRESS;
}
